package ebf.communityban;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

class BannedPlayer {
    UUID account =null;
    String name=null;
    int unbanYear, unbanDay,unbanHour,unbanMinute;
    List<String> ipAddress=new ArrayList<>();
    List<String> bypassRoles=null;

    public String writeString() {
        CommunityBan.builder = new StringBuilder();
        CommunityBan.builder.append("Name[");
        CommunityBan.builder.append(name);
        CommunityBan.builder.append("] Ban Time [");
        CommunityBan.builder.append(unbanYear);
        CommunityBan.builder.append(".");
        CommunityBan.builder.append(unbanDay);
        CommunityBan.builder.append(".");
        CommunityBan.builder.append(unbanHour);
        CommunityBan.builder.append(".");
        CommunityBan.builder.append(unbanMinute);
        CommunityBan.builder.append("] UUID [");
        CommunityBan.builder.append(account.toString());
        CommunityBan.builder.append("] IP Address [");
        if (ipAddress != null) {
            for (String ip : ipAddress) {
                CommunityBan.builder.append(ip);
                CommunityBan.builder.append(",");
            }
        }
        CommunityBan.builder.append("]}");
        return CommunityBan.builder.toString();
    }


    public BannedPlayer readString(String data){
        String currentParse= data.substring(data.indexOf("{[")+1);
        name=currentParse.substring(0,currentParse.indexOf("] Ban Time ["));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);
        String[] bantime = currentParse.substring(0,currentParse.indexOf("] UUID [")).split(".");
        
        
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);
        String uuid= = currentParse.substring(0,currentParse.indexOf("] IP Address ["));
        currentParse=currentParse.substring(currentParse.indexOf("[")+1);

        split = currentParse.substring(0,currentParse.indexOf("]}")).split(",");
        bypassRoles.addAll(Arrays.asList(split));

        return this;
    }

}
